#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>

struct cec_framedata {
	char srcdestaddr;
	char opcode;
	char args[16];
	char argcount;
	char returnval;
};

#define HDMI_CEC_MAGIC 'N'
#define HDMI_IOCTL_CECSEND   _IOW(HDMI_CEC_MAGIC, 0 ,struct cec_framedata)
#define HDMI_IOCTL_CECENAB   _IOW(HDMI_CEC_MAGIC, 1, int)
#define HDMI_IOCTL_CECPHY    _IOR(HDMI_CEC_MAGIC, 2, int)
#define HDMI_IOCTL_CECLOGIC  _IOR(HDMI_CEC_MAGIC, 3, int)
#define HDMI_IOCTL_CECREAD   _IOR(HDMI_CEC_MAGIC, 4, struct cec_framedata)
#define HDMI_IOCTL_CECSETLA  _IOW(HDMI_CEC_MAGIC, 5, int)
#define HDMI_IOCTL_CECCLEARLA  _IOW(HDMI_CEC_MAGIC, 6, int)
#define HDMI_IOCTL_CECWAKESTATE  _IOR(HDMI_CEC_MAGIC, 7, int)

enum {
	CEC_SEND_SUCCESS = 0,
	CEC_SEND_NACK,
	CEC_SEND_BUSY
};

void cechelp(void)
{
	printf( "*******************************************************************\n");
	printf( "       Rockchip : HDMI-CEC test file\n");
	printf( "       CECTEST DEFAULT FUNCTION -- one touch play\n");
	printf( "       user input:cec\n\n" );
	printf( "       USER CAN INPUT CMD BY FOLLOW FORMAT:\n");
	printf( "       cec src(MSB4bit)dest(LSB4bit) opcode paracount para[]\n");
	printf( "       for example: TV stanby\n       cec 0x40 0x36 0x0\n");
	printf( "*******************************************************************\n");
}

int main(int argc, char** argv)
{
	int fd = -1, fdstat = -1;
	struct cec_framedata cecframe;
	struct cec_framedata send_frame;
	int phy_addr, log_addr;
	int ret, x;

	/*
	printf("argc = %d\n", argc);
	fdstat = argc;
	for (fdstat = 1; fdstat < argc; fdstat++)
		printf ("agrv[%d] = %s\n", fdstat, argv[fdstat] );
	*/

	fd = open("/dev/cec", O_RDWR, 0);
	if (fd < 0) {
		printf("failed to open cec device\n");
		return -errno;
	}

	ret = ioctl(fd, HDMI_IOCTL_CECPHY, &phy_addr);
	if (ret) {
		printf("CEC read physical addr error!\n");
		close(fd);
		return -errno;
	}

	//printf("CEC physical address is 0x%x\n", phy_addr);
	if (argc == 1) {
		if (phy_addr > 0){
			log_addr = 4;
			ret = ioctl(fd, HDMI_IOCTL_CECSETLA, &log_addr);
			if (ret) {
				printf("CEC  set logical adddr error!\n");
				close(fd);
				return -errno;
			}
			cecframe.srcdestaddr = 0x40;
			cecframe.opcode = 0x04;
			cecframe.argcount = 0;
			ret = ioctl(fd, HDMI_IOCTL_CECSEND, &cecframe);
			if (ret) {
				printf("CEC send Image view error\n");
				close(fd);
				return -errno;
			}

			cecframe.srcdestaddr = 0x4f;
			cecframe.opcode = 0x82;
			cecframe.argcount = 2;
			cecframe.args[0] = (phy_addr & 0xFF00) >> 8;
			cecframe.args[1] = (phy_addr & 0x00FF);
			ret = ioctl(fd, HDMI_IOCTL_CECSEND, &cecframe);
			if (ret) {
				printf("CEC send active source error\n");
				close(fd);
				return -errno;
			}
		}
	} else if (argc == 2) {
		if (!strcmp("help", argv[1]))
			cechelp();
	} else {
		send_frame.srcdestaddr = strtol(argv[1],NULL,16);
		log_addr = (send_frame.srcdestaddr >> 4) & 0xf;
		printf("user set logical addr is = 0x%x\n", log_addr);
		ret = ioctl(fd, HDMI_IOCTL_CECSETLA, &log_addr);
		if (ret) {
			printf("CEC  set logical adddr error!\n");
			close(fd);
			return -errno;
		}
		send_frame.opcode = strtol(argv[2], NULL, 16);
		send_frame.argcount = strtol(argv[3], NULL, 16);
		printf("0x%x, 0x%x, 0x%x\n", send_frame.srcdestaddr, send_frame.opcode, send_frame.argcount);
		for (x = 0; x < send_frame.argcount; x++)
			send_frame.args[x] = strtol(argv[4 + x], NULL, 16);;
		ret = ioctl(fd, HDMI_IOCTL_CECSEND, &send_frame);
		if (ret) {
			printf("CEC send error\n");
			close(fd);
			return -errno;
		}

	}
	close(fd);
	exit(0);
}
