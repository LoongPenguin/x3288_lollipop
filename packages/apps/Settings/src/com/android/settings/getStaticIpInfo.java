package com.android.settings;

public interface getStaticIpInfo {
	public boolean getStaticIp(String ipAddr);
	public boolean getStaticNetMask(String netMask);
	public boolean getStaticGateway(String gateway);
	public boolean getStaticDns1(String dns1);
	public boolean getStaticDns2(String dns2);
    public boolean getEthHostname(String hostname);
    public boolean getEthPort(String port);
    public boolean getEthExclusionlist(String exclusionlist);
    public boolean getEthPac(String pac);
    public boolean getEthProxySettings(String proxySettings);
}
